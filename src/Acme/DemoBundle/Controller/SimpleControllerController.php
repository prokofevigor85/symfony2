<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SimpleControllerController extends Controller
{
    /**
     * @Route("/calc/{divider}/{limit}")
     * @Template()
     */
    public function calculateSummAction($divider, $limit)
    {
        $result = 0;
        if(is_numeric($divider) && is_numeric($limit)){
            while($limit > 0){
                if(($limit % $divider) == 0){
                    $result += $limit;
                }
                $limit--;
            }
        }
        return new \Symfony\Component\HttpFoundation\Response($result);
    }

}
